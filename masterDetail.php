<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
  
<script type="text/javascript" charset="utf8" src="js/jquery-1.9.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
<div style='width:809px;margin:auto'>
	<h3>Provinsi</h3>
	<table id="province" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>ID</th>
				<th>NAMA</th>
				<th>ACTION</th>
			</tr>
		</thead>
	</table>
<hr/>
	<h3>Kota</h3>
	<table id="district" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>ID</th>
				<th>SPEK</th>
				<th>KETERANGAN</th>
			</tr>
		</thead>
	</table>
</div>
<hr/>
<script>
$(document).ready(function() {
    tableProvinsi=$('#province').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":  "scripts/province.php",
            "type": "POST"
        }, 
    } );
	tableKota=$('#district').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":  "scripts/district.php",
            "type": "POST"
        }, 
    } );
} );
function showDetail(id){
	tableKota.ajax.url('scripts/district.php?id_province='+id);
	tableKota.load();
	
}
</script>

