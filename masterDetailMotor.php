<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
  
<script type="text/javascript" charset="utf8" src="js/jquery-1.9.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
<div style='width:809px;margin:auto'>
	<h3>MOTOR</h3>
	<table id="motor" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>ID</th>
				<th>NAMA</th>
				<th>ACTION</th>
			</tr>
		</thead>
	</table>
<hr/>
	<h3>DETAIL</h3>
	<table id="Detail" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>ID</th>
				<th>SPEK</th>
				<th>KETERANGAN</th>
			</tr>
		</thead>
	</table>
</div>
<hr/>
<script>
/*
var table = $('#example').DataTable( {
    ajax: "data.json"
} );
 
table.ajax.url( 'newData.json' ).load();
*/
$(document).ready(function() {
    motor=$('#motor').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":  "scripts/motor.php",
            "type": "POST"
        }, 
    } );
	motorDetail=$('#Detail').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":  "scripts/detail.php",
            "type": "POST"
        }, 
    } );
} );
function showDetail(id){
	motorDetail.ajax.url('scripts/detail.php?id_motor='+id);
	motorDetail.load();
	
}
</script>

