CREATE TABLE IF NOT EXISTS `province` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `notes` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `name` (`name`);
ALTER TABLE `province` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
CREATE TABLE IF NOT EXISTS `district` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_province` int(11) NOT NULL,
  `notes` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `name` (`name`),
  ADD KEY `id_province` (`id_province`);
ALTER TABLE `district` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;